$(function() {
    'use strict';
    var $page = $('#main'),
        options = {
            debug: true,
            prefetch: true,
            cacheLength: 2,
            blacklist: '.no-ss',
            onReady: {
                duration: 0,
                render: function($container, $newContent) {
                    // Inject the new content
                    $container.html($newContent);
                }
            },
            // over a year ago, this was simply callback: function(){}
            onAfter: function($container, $newContent) {
                runTeam();
                runMenu();
            }
        },
        smoothState = $page.smoothState(options).data('smoothState');
});
