// Functions

// Menu
function runMenu() {
  var $menuToggles = '.mobile-navigation-toggle, .mobile-navigation-close';
  var $nav = $('header nav');
  $('body').on('click',$menuToggles,function() { $nav.toggleClass('open'); })
}

// Team Page
function runTeam() {
  $('.team-members').imagesLoaded(function(){
    $('.team-members').isotope({
      itemSelector: '.team-member',
      layoutMode: 'masonry'
    })
  })
}
